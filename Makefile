VUNDLE = https://github.com/VundleVim/Vundle.vim.git

all: clean get_deps copyrc copydir

get_deps:
	sudo apt-get install git vim build-essential cmake g++ tmux htop
	git clone $(VUNDLE) vim/autoload/Vundle.vim
clean:
	-@rm -rf vim/autoload/*
	-@rm ~/.vimrc
	-@rm ~/.vim -rf
copyrc:
	-@cp vimrc ~/.vimrc
copydir:
	-@cp -r vim ~/.vim
