rm -rf vim/autoload/*
VUNDLE="https://github.com/VundleVim/Vundle.vim.git"
git clone $VUNDLE vim/autoload/Vundle.vim
rm -rf ~/.vimrc
rm -rf ~/.vim
cp vimrc ~/.vimrc
cp -r vim ~/.vim
