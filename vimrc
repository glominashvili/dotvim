set nocompatible
set encoding=utf-8
filetype off

set rtp+=~/.vim/autoload/Vundle.vim
call vundle#begin()

Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/syntastic'
Plugin 'mtscout6/syntastic-local-eslint.vim'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'Shougo/unite.vim'
Plugin 'Shougo/vimfiler.vim'
Plugin 'maksimr/vim-jsbeautify'
Plugin 'tpope/vim-surround'
Plugin 'jiangmiao/auto-pairs'
Plugin 'sotte/presenting.vim'
Plugin 'bling/vim-airline'
Plugin 'vim-scripts/AutoComplPop'
Plugin 'ryanoasis/vim-devicons'
Plugin 'mhinz/vim-startify'
Plugin 'tpope/vim-fugitive'
Plugin 'posva/vim-vue'
Plugin 'tomlion/vim-solidity'
Plugin 'isRuslan/vim-es6'
Plugin 'sheerun/vim-polyglot'
Plugin 'joshdick/onedark.vim'

call vundle#end()

filetype plugin on
filetype plugin indent on

syntax on
set exrc
set secure
set number
set laststatus=1
set mouse=a
set autoindent
set smartindent
set cindent 
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set linebreak
set showbreak=>
set mousehide       
set backspace=indent,eol,start
set copyindent
set showmatch
set smarttab
set hlsearch
set incsearch
set history=1000
set undolevels=1000
set cursorline
set ruler
set guioptions=aAe
set nobackup
set noswapfile
set hidden
set relativenumber
colorscheme onedark

let g:html_indent_inctags = "html,head,body,canvas,script,div,p"
let g:vimfiler_as_default_explorer = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_detect_modified=1
let g:airline_detect_paste=1
let g:airline_detect_iminsert=1
let g:airline_powerline_fonts = 1

" syntactic
let g:airline#extensions#syntastic#enabled = 1
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_loc_list_height = 5
"let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 1
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_error_symbol = ''
let g:syntastic_style_error_symbol = '!?'
let g:syntastic_warning_symbol = ''
let g:syntastic_style_warning_symbol = ''
highlight link SyntasticErrorSign SignColumn
highlight link SyntasticWarningSign SignColumn
highlight link SyntasticStyleErrorSign SignColumn
highlight link SyntasticStyleWarningSign SignColumn


map gb :bnext<cr>
map gB :bprevious<cr>

nnoremap <leader>w :w!<cr>
nnoremap <leader>r :redo<cr>
nnoremap <leader>qa :qa!<cr>
nnoremap <leader>q :q<cr>
nnoremap <leader>b :Kwbd<CR>
nnoremap <leader>t :tabclose<cr>
nnoremap <leader>i gg=G ''
nnoremap <tab> <C-w><C-w>
nnoremap <leader>uf :Unite file<cr>
nnoremap <leader>ub :Unite buffer<cr>
nnoremap <leader>uu :Unite file buffer<cr>
nnoremap <leader>ff :VimFiler<cr>
nnoremap <leader>fe :VimFilerExplorer -winwidth=50<cr>

noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

set wildmenu
set clipboard=unnamedplus

if has('gui_running')
  set guioptions-=m  "remove menu bar
  set guioptions-=T  "remove toolbar
  set guioptions-=r  "remove right-hand scroll bar
  set guioptions-=L  "remove left-hand scroll bar
else
endif

let $VIMHOME=expand('<sfile>:p:h:h')
